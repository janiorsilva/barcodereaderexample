package view;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileSystemView;
import com.google.zxing.NotFoundException;

import barcodereader.MyBarcodeReader;

public class MyMainScreen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String meuArquivo = "";
		try {
			String diretorioInicial = "D:\\IFMG\\Projetos\\integrado\\bracode readers\\images";
			//diretorioInicial = FileSystemView.getFileSystemView().getHomeDirectory();
			JFileChooser jfc = new JFileChooser(diretorioInicial);
			int returnValue = jfc.showOpenDialog(null);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				File selectedFile = jfc.getSelectedFile();
				System.out.println(selectedFile.getAbsolutePath());
				meuArquivo = selectedFile.getAbsolutePath();
				String codigoLido = MyBarcodeReader.readQRCode(meuArquivo);
				//String codigoLido = MyBarcodeReader.readBarCode(meuArquivo);
				JOptionPane.showMessageDialog(null, "C�digo lido: " + codigoLido);
				System.out.println("C�digo lido: " + codigoLido);
			}
		} catch (NotFoundException | IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Erro: " + e.getMessage());
			e.printStackTrace();
		}

	}
}
